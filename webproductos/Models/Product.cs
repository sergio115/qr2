﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webproductos
{
    public class Product
    {
        public int Id { get; set; }
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
        public int Orden { get; set; }
        public int Contador { get; set; }
        public int Sscc { get; set; }
        public int Lote { get; set; }
    }
}
