﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace webproductos
{
    public class WebAPI
    {
        public static Product SearchById(int IdProduct) {
            Product InformationProduct = new Product();
            DataTable _query = new DataTable();

            try
            {
                _query = DBConn.Query($" select * from producto where id = {IdProduct}");
                if (_query.Rows.Count > 0)
                {
                    InformationProduct.Id = Convert.ToInt32(_query.Rows[0].ItemArray[0]);
                    InformationProduct.Codigo = Convert.ToInt32(_query.Rows[0].ItemArray[1]);
                    InformationProduct.Descripcion = _query.Rows[0].ItemArray[2].ToString();
                    InformationProduct.Orden = Convert.ToInt32(_query.Rows[0].ItemArray[3]);
                    InformationProduct.Contador = Convert.ToInt32(_query.Rows[0].ItemArray[4]);
                    InformationProduct.Sscc = Convert.ToInt32(_query.Rows[0].ItemArray[5]);
                    InformationProduct.Lote = Convert.ToInt32(_query.Rows[0].ItemArray[6]);
                }
            }
            catch (Exception)
            {
                InformationProduct.Id = 0;
                InformationProduct.Codigo = 0;
                InformationProduct.Descripcion = "cero";
                InformationProduct.Orden = 0;
                InformationProduct.Contador = 0;
                InformationProduct.Sscc = 0;
                InformationProduct.Lote = 0;
            }

            return InformationProduct;
        }

        public static Product Create(Product InformationProduct)
        {
            //Product InformationProduct = new Product();
            DataTable _query = new DataTable();

            try
            {
                _query = DBConn.Query($" INSERT INTO producto (codigo, descripcion, orden, contador, sscc, lote)" +
                                      $" VALUES ('{InformationProduct.Codigo}', '{InformationProduct.Descripcion}'," +
                                      $" '{InformationProduct.Orden}', '{InformationProduct.Contador}'," +
                                      $" '{InformationProduct.Sscc}', '{InformationProduct.Lote}')");
            }
            catch (Exception)
            {
                InformationProduct.Id = 0;
                InformationProduct.Codigo = 0;
                InformationProduct.Descripcion = "cero";
                InformationProduct.Orden = 0;
                InformationProduct.Contador = 0;
                InformationProduct.Sscc = 0;
                InformationProduct.Lote = 0;
            }

            return InformationProduct;
        }
    }
}
