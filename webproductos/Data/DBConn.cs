﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace webproductos
{
    public class DBConn
    {
        public static SqlConnection ConnectionSQL() {
            SqlConnection ConectString = new SqlConnection("Data Source=LAPTOP-PEREZ\\SQLEXPRESS;" +
                                                           "Initial Catalog=productos;" + //Base de datos
                                                           "User id=sa;" +
                                                           "Password=98898998;");
            return ConectString;
        }

        public static DataTable Query(string query) {
            DataTable _Query = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn = ConnectionSQL();
            SqlCommand cmd = new SqlCommand(query, conn);

            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                else
                {
                    conn.Open();
                }
                _Query.Load(cmd.ExecuteReader());
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                conn.Close();
            }
            
            return _Query;
        }
    }
}
