﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace webproductos
{
    [Route("api/[controller]")]
    public class SearchController : Controller
    {
        // GET: api/<controller>
        [HttpGet("{productId}")]
        public string Search(int productId) {
            Product InformationProduct = new Product();
            try
            {
                InformationProduct = WebAPI.SearchById(productId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            var jsonConvert = JsonConvert.SerializeObject(InformationProduct);
            return jsonConvert.ToString();
        }

        [HttpPost]
        public string New([FromBody]Product product) {
            //Product InformationProduct = new Product();
            try
            {
                //InformationProduct = WebAPI.SearchById(productId);
                WebAPI.Create(product); // El ID no lo obtiene y retorna el que se metio(el cual no hace nada en la consulta)
            }
            catch (Exception ex)
            {
                throw ex;
            }
            var jsonConvert = JsonConvert.SerializeObject(product);
            return jsonConvert.ToString();

        }

        /*
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        */
    }
}
