﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using QR2.Services;
using Xamarin.Forms;
using ZXing.Mobile;

[assembly: Dependency(typeof(QR2.Droid.Services.QrCodeScanningService))]
namespace QR2.Droid.Services
{
    public class QrCodeScanningService : IQrCodeScanningService
    {
        public QrCodeScanningService()
        {

        }

        public async Task<string> ScanAsync()
        {
            var optionsDefault = new MobileBarcodeScanningOptions();
            var optionsCustom = new MobileBarcodeScanningOptions()
            {

            };
            var scanner = new MobileBarcodeScanner()
            {
                TopText = "Acerca la cámara al elemento",
                BottomText = "Toca la pantalla para enfocar"
            };
            var scanResults = await scanner.Scan(optionsCustom);
            return (scanResults != null) ? scanResults.Text : string.Empty;

            throw new NotImplementedException();
        }
    }
}