﻿using QR2.Data;
using QR2.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QR2
{
    public partial class App : Application
    {
        public static ProductoManager ProductManager { get; private set; }

        public App()
        {
            InitializeComponent();

            ProductManager = new ProductoManager(new RestService());
            MainPage = new MyPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
