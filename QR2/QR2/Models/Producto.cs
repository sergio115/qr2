﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR2.Services
{
    public class Producto
    {
        public int id { get; set; }
        public int codigo { get; set; }
        public string descripcion { get; set; }
        public int orden { get; set; }
        public int contador { get; set; }
        public Int64 sscc { get; set; }
        public int lote { get; set; }
    }
}
