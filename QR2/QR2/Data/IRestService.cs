﻿using QR2.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QR2.Data
{
    public interface IRestService
    {
        Task SaveProductoAsync(Producto item);
    }
}
