﻿using QR2.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QR2.Data
{
    public class ProductoManager
    {
        IRestService restService;

        public ProductoManager(IRestService service)
        {
            restService = service;
        }

        public Task SaveTaskAsync(Producto producto)
        {
            return restService.SaveProductoAsync(producto);
        }
    }
}
