﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using QR2.Services;

namespace QR2.Data
{
    public class RestService : IRestService
    {
        HttpClient client;
        public const string url = "http://192.168.0.17/webproductos/prod.php";

        public RestService()
        {
            client = new HttpClient();
        }

        public async Task SaveProductoAsync(Producto item)
        {
            var uri = new Uri(string.Format(url, string.Empty));

            try
            {
                var json = JsonConvert.SerializeObject(item);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                response = await client.PostAsync(uri, content);
                
                if (response.IsSuccessStatusCode)
                {
                    Debug.WriteLine(@"				TodoItem successfully saved.");
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
        }
    }
}
