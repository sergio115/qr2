﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QR2.Services
{
    public interface IQrCodeScanningService
    {
        Task<string> ScanAsync();
    }
}
