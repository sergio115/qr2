﻿using Newtonsoft.Json;
using QR2.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QR2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyPage : ContentPage
    {
        //private const string url = "http://192.168.0.17/webproductos/prod.php";
        //private HttpClient _Client = new HttpClient();
        //private ObservableCollection<Producto> _post;

        public MyPage()
        {
            InitializeComponent();
        }

        private async void BtnScan_Clicked(object sender, EventArgs e)
        {
            var scanner = DependencyService.Get<IQrCodeScanningService>();
            var result = await scanner.ScanAsync();
            if (result != null)
            {
                barCode.Text = result;
            }
        }

        private async void BtnSend_Clicked(object sender, EventArgs e)
        {
            var product = (Producto)BindingContext;
            await App.ProductManager.SaveTaskAsync(product);


            //var uri = new Uri(string.Format(url, string.Empty));
            //var post = new Producto();
            //post.codigo = Convert.ToInt32(code.Text); post.descripcion = description.Text;
            //post.orden = Convert.ToInt32(order.Text); post.contador = Convert.ToInt32(counter.Text);
            //post.sscc = Convert.ToInt64(barCode.Text); post.lote = Convert.ToInt32(lot.Text);

            //var json = JsonConvert.SerializeObject(post);
            //var content = new StringContent(json, Encoding.UTF8, "application/json");
            //HttpResponseMessage response = null;

            //response = await _Client.PostAsync(uri, content);

            //if (response.IsSuccessStatusCode)
            //{
            //    Debug.WriteLine(@"                TodoItem successfully saved.");

            //}
        }
    }
}